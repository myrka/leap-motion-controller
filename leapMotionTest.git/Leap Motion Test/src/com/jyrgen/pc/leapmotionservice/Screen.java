package com.jyrgen.pc.leapmotionservice;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;

import javax.swing.JWindow;

import com.jyrgen.pc.leapmotionservice.render.Renderer;


public class Screen extends JWindow{
	
	private static final long serialVersionUID = 1L;
	public LeapMotionTestMain main;
	public Renderer renderer;
	public Rectangle defaultScreen;
	
	public Screen(LeapMotionTestMain main) {
		this.main = main;
		renderer = new Renderer(this);
		Rectangle size = getScreenBounds();
		setAlwaysOnTop(true);
		setBounds(size);
		setBackground(new Color(0, true));
		setVisible(true);
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		//System.out.print("p");
		if(main.listener == null  ||  main.listener.getLastControllerInfo() == null){
			System.out.println("Just starting, something is still null");
		}else if(main.listener.getLastControllerInfo().isConnected()){
			//Leap Motion Controller is connected!
			renderer.render(g, main.listener.getLastControllerInfo());
		}else{
			//Leap Motion Controller is NOT connected!
			g.setColor(Color.RED);
			g.setFont(getFont().deriveFont(48f));
			g.drawString("Controller is not connected!", 10, 100);
			System.out.println("Controller is not connected!");
		}
	}

	@Override
	public void update(Graphics g) {
		paint(g);
	}
	
	private Rectangle getScreenBounds() {
		double minX, maxX, minY, maxY;
		defaultScreen = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getBounds();

		minX = defaultScreen.getX();
		maxX = defaultScreen.getMaxX();
		minY = defaultScreen.getY();
		maxY = defaultScreen.getMaxY();

		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] gs = ge.getScreenDevices();
		for (GraphicsDevice curGs : gs) {
			GraphicsConfiguration[] gc = curGs.getConfigurations();
			for (GraphicsConfiguration curGc : gc) {
				Rectangle bounds = curGc.getBounds();
				if (minX > bounds.getX())
					minX = bounds.getX();
				if (maxX < bounds.getMaxX())
					maxX = bounds.getMaxX();
				if (minY > bounds.getY())
					minY = bounds.getY();
				if (maxY < bounds.getMaxY())
					maxY = bounds.getMaxY();
			}
		}
		return new Rectangle((int)minX, (int)minY, (int)maxX, (int)maxY);
	}
}