package com.jyrgen.pc.leapmotionservice.jna;

import java.util.ArrayList;
import java.util.List;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinNT.HANDLE;

public class MediaPlayerController {
	
	private static final User32 user32 = User32.INSTANCE;
	
	private static final int 	WM_APPCOMMAND 	= 0x0319;
	private static final int 	HWND_BROADCAST	= 0xFFFF;
	
	private static final int 	APPCOMMAND_MEDIA_PREVIOUSTRACK	= 0xC1000;
	private static final int 	APPCOMMAND_MEDIA_NEXTTRACK		= 0xB1000;
	private static final int 	APPCOMMAND_MEDIA_PLAY_PAUSE		= 0xE1000;
	
	public static List<Pointer> getAllWindowHwnds() {
		final List<Pointer> windowHwnds = new ArrayList<Pointer>();
		user32.EnumWindows(new User32.WNDENUMPROC() {
			@Override
			public boolean callback(Pointer hWnd, Pointer arg) {
				if (user32.IsWindowVisible(hWnd)) {
					windowHwnds.add(hWnd);
				}
				return true;
			}
		}, null);
		return windowHwnds;
	}
	
	private static String getWindowText(Pointer hWnd) {
		int nMaxCount = 512;
		byte[] lpString = new byte[nMaxCount];
		int result = user32.GetWindowTextA(hWnd, lpString, nMaxCount);
		if (result == 0) {
			return "";
		}
		return Native.toString(lpString);
	}
	
	private boolean PostMessage(Pointer hWnd, int Msg, int wParam, int lParam){
		System.out.println("Saadan k�su");
		return user32.PostMessageA(new HANDLE(hWnd), Msg, new Pointer(wParam), new Pointer(lParam));
	}

	/**
	 * Mingisugusel imelikul p�hjusel l�hevad need s�numid igale aknale laiali,
	 * ka siis, kui saadad kindlale aknale. Seega see funktsioon saadab �hele
	 * suvalisele aknale commandi ja loodab, et see j�uab igale poole
	 * */
	private void postMessageToOneRandomWindow(int lParam){
		List<Pointer> aknad = getAllWindowHwnds();
		boolean found = false;
		for(Pointer p : aknad){
			String s = getWindowText(p);
			if(s.startsWith("Spotify")){
				System.out.println("Leidsin Spotify!" + s);
				PostMessage(p, WM_APPCOMMAND, 0, lParam);
				found = true;
				break;
			}
		}
		if(!found){
			//ei leitud, saadan k�igile
			PostMessage(new Pointer(HWND_BROADCAST), WM_APPCOMMAND, 0, lParam);
		}
		/*if(aknad.size() > 0){
			PostMessage(aknad.get(0), WM_APPCOMMAND, 0, lParam);
		}else{
			PostMessage(new Pointer(HWND_BROADCAST), WM_APPCOMMAND, 0, lParam);
		}*/
	}
	
	public void playPause(){
		postMessageToOneRandomWindow(APPCOMMAND_MEDIA_PLAY_PAUSE);
	}
	
	public void previousTrack(){
		postMessageToOneRandomWindow(APPCOMMAND_MEDIA_PREVIOUSTRACK);
	}
	
	public void nextTrack(){
		postMessageToOneRandomWindow(APPCOMMAND_MEDIA_NEXTTRACK);
	}
}
