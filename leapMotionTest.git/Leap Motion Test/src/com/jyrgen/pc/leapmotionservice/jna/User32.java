package com.jyrgen.pc.leapmotionservice.jna;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.win32.StdCallLibrary;

public interface User32 extends StdCallLibrary {
   User32 INSTANCE = (User32) Native.loadLibrary("user32", User32.class);

	interface WNDENUMPROC extends StdCallCallback {
		boolean callback(Pointer hWnd, Pointer arg);
	}
	
	boolean EnumWindows(WNDENUMPROC lpEnumFunc, Pointer userData);
	
	int GetWindowTextA(Pointer hWnd, byte[] lpString, int nMaxCount);
	
	boolean IsWindowVisible(Pointer hWnd);
	
	boolean PostMessageA(HANDLE hWnd, int Msg, Pointer wParam, Pointer lParam);
   
}