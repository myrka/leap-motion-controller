package com.jyrgen.pc.leapmotionservice;

public class Vocabulary {
	public static final int ROOM = 0;
	public static final int MUSIC = 1;
	public static final int LOCK = 2;
	public static final int LIGHT = 3;
	public static final int DARK = 4;
	public static final int CLOSE_DOOR = 5;
	public static final int PREVIOUS = 6;
	public static final int NEXT = 7;
	public static final int PLAY_PAUSE = 8;
	public static final int ARE_YOU_SURE = 9;
	public static final int YES = 10;
	public static final int NO = 11;
	/*
	public static final int  = 12;
	public static final int  = 13;
	public static final int  = 14;
	public static final int  = 15;
	*/
	
	private static final String LANGUAGE = "ENGLISH";
	
	private static final String estonian[] 	= {"Tuba", 	"Muusika", 	"Lukusta", 	"Valgus", 	"Pimedus", 	"Uks kinni", 	"Eelmine", 	"J�rgmine",	"Play/Pause", 	"Oled kindel?", 	"Jah", 	"Ei"};
	private static final String english[] 	= {"Room", 	"Music", 	"Lock", 	"Light", 	"Dark", 	"Close door", 	"Previous",	"Next", 	"Play/Pause", 	"Are you sure?", 	"Yes", 	"No"};
	
	public static String get(int word){
		if(LANGUAGE.equals("ESTONIAN")) {
			return estonian[word];
		}else if(LANGUAGE.equals("ENGLISH")) {
			return english[word];
		}else {
			return "Language not specified!";
		}
	}
	
}
