package com.jyrgen.pc.leapmotionservice.render;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

import com.jyrgen.pc.leapmotionservice.LeapMotionTestMain;
import com.jyrgen.pc.leapmotionservice.Screen;
import com.jyrgen.pc.leapmotionservice.Vocabulary;
import com.leapmotion.leap.CircleGesture;
import com.leapmotion.leap.Controller;
import com.leapmotion.leap.Finger;
import com.leapmotion.leap.Gesture;
import com.leapmotion.leap.Gesture.State;
import com.leapmotion.leap.Gesture.Type;
import com.leapmotion.leap.GestureList;
import com.leapmotion.leap.Hand;

public class Renderer {
	
	private Screen screen;
	private LeapMotionTestMain main;
	private long stateStartTime = System.currentTimeMillis();
	
	public boolean isUnlocking = false;
	
	public static final int LAG_REMOVER_THRESHOLD = 20;
	
	public enum States {
		STATE_IDLE, 
		STATE_SHOW_CURSOR_POSITION,
		STATE_ENTER_MENU,
		STATE_SHOW_MAIN_MENU,
		STATE_SHOW_ROOM_MENU,
		STATE_SHOW_MUSIC_MENU,
		STATE_CONFIRM_LOCK,
		STATE_LOCKED;
	}
	public States currentState = States.STATE_IDLE, lastState = States.STATE_IDLE;
	
	private Cursor cur;
	
	
	//temp test
	int i = 0;
	
	public Renderer(Screen screen) {
		this.screen = screen;
		this.main = screen.main;
		cur = new Cursor(main);
	}
	
	public boolean shouldPaintFast = false;
	public void render(Graphics g, Controller controller){
		//System.out.println("RENDER");
		//enamikel juhtudel on vaja kiiret andmetet��tlemist
		
		System.out.println("render " + shouldPaintFast);
		shouldPaintFast = true;
		if(currentState == States.STATE_IDLE){
			shouldPaintFast = false;
			System.out.println("IDLE2 - VOILA!");
		}else if(currentState == States.STATE_LOCKED  &&  !isUnlocking){
			System.out.println("LOCK2");
			shouldPaintFast = false;
		}
		
		switch(currentState){
		case STATE_IDLE:
			//kui programmi ei kasutata, siis v�tame arvutilt suurema koormuse maha
			stateIdle(g, controller);
			break;
		case STATE_SHOW_CURSOR_POSITION:
			stateShowCursorPosition(g, controller);
			break;
		case STATE_ENTER_MENU:
			stateEnterMenu(g, controller);
			break;
		case STATE_SHOW_MAIN_MENU:
			stateShowMainMenu(g, controller);
			break;
		case STATE_SHOW_MUSIC_MENU:
			stateShowMusicMenu(g, controller);
			break;
		case STATE_SHOW_ROOM_MENU:
			stateShowRoomMenu(g, controller);
			break;
		case STATE_CONFIRM_LOCK:
			stateConfirmLock(g, controller);
			break;
		case STATE_LOCKED:
			stateLocked(g, controller);
			break;
		default:
			
			break;
		}
	}
	
	public void setState(States state){
		stateStartTime = System.currentTimeMillis();
		lastState = currentState;
		currentState = state;
		System.out.println("State changed, now " + state.toString());
	}
	int ii = 0;
	public void stateIdle(Graphics g, Controller c){
		if(c.frame().hands().count() > 0){
			boolean isExtended[] = new boolean[5];
			for(Hand hand : c.frame().hands()) {
				for(Finger finger : hand.fingers()){
					if(finger.type() == Finger.Type.TYPE_THUMB)
						isExtended[0] = finger.isExtended();
					else if(finger.type() == Finger.Type.TYPE_INDEX)
						isExtended[1] = finger.isExtended();
					else if(finger.type() == Finger.Type.TYPE_MIDDLE)
						isExtended[2] = finger.isExtended();
					else if(finger.type() == Finger.Type.TYPE_RING)
						isExtended[3] = finger.isExtended();
					else if(finger.type() == Finger.Type.TYPE_PINKY)
						isExtended[4] = finger.isExtended();
					
					if(finger.type() == Finger.Type.TYPE_INDEX){
						cur.calculatePosition(finger);
					}
				}
			}
			int x = (int) (screen.defaultScreen.getMaxX() - screen.defaultScreen.getX());
			int y = (int) (screen.defaultScreen.getMaxY() - screen.defaultScreen.getY());
			double distFromCenter = Math.sqrt((x/2-cur.getX())*(x/2-cur.getX())  +  (y/2-cur.getY())*(y/2-cur.getY()));
			
			if(lastState == States.STATE_ENTER_MENU  &&  distFromCenter > screen.defaultScreen.getWidth()/3){
				
			}else if(isExtended[0] == true  &&  isExtended[1] == true  &&  isExtended[2] == false  &&  isExtended[3] == false  &&  isExtended[4] == false){
				//l�hme edasi vaid siis, kui p�ial ja INDEX on v�lja sirutatud
				setState(States.STATE_SHOW_CURSOR_POSITION);
			}
		}else if(lastState != States.STATE_IDLE){
			setState(States.STATE_IDLE);
		}
	}
	
	private long unlockStateChangeTime = System.currentTimeMillis();
	public void stateLocked(Graphics g, Controller c){
		//g.drawString("LOCKED!", 200, 200);
		isUnlocking = false;
		float unlockProgress = 0;
		float unlockSpeed = 0;
		if(c.frame().hands().count() > 0){
			boolean isExtended[] = new boolean[5];
			for(Hand hand : c.frame().hands()) {
				for(Finger finger : hand.fingers()){
					if(finger.type() == Finger.Type.TYPE_THUMB)
						isExtended[0] = finger.isExtended();
					else if(finger.type() == Finger.Type.TYPE_INDEX)
						isExtended[1] = finger.isExtended();
					else if(finger.type() == Finger.Type.TYPE_MIDDLE)
						isExtended[2] = finger.isExtended();
					else if(finger.type() == Finger.Type.TYPE_RING)
						isExtended[3] = finger.isExtended();
					else if(finger.type() == Finger.Type.TYPE_PINKY)
						isExtended[4] = finger.isExtended();
				}
			}
			
			GestureList gestures = c.frame().gestures();
	        for (int i = 0; i < gestures.count(); i++) {
	            Gesture gesture = gestures.get(i);
	            if(gesture.type() == Type.TYPE_CIRCLE){
	            	if(isExtended[0] == false  &&  isExtended[1] == true  &&  isExtended[2] == false  &&  isExtended[3] == false  &&  isExtended[4] == false){
		            	CircleGesture circle = new CircleGesture(gesture);
		            	if (circle.pointable().direction().angleTo(circle.normal()) <= Math.PI/2) {
		            		if(gesture.state() == State.STATE_START){
			            		unlockStateChangeTime = System.currentTimeMillis();
			            	}else if(gesture.state() == State.STATE_STOP){
			            		unlockStateChangeTime = System.currentTimeMillis();
			            	}
			            	// Calculate angle swept since last frame
		                    double sweptAngle = 0;
		                    if (circle.state() != State.STATE_START) {
		                        CircleGesture previousUpdate = new CircleGesture(c.frame(1).gesture(circle.id()));
		                        sweptAngle = (circle.progress() - previousUpdate.progress()) * 2 * Math.PI;
		                    }
		                    isUnlocking = true;
		                    System.out.println("Sa avad lukku progressiga " + circle.progress() + " \t ja kiirusega " + Math.toDegrees(sweptAngle));
		                    unlockProgress = Math.min(1f, circle.progress()/10f);
		                    unlockSpeed = (float) Math.toDegrees(sweptAngle);
		                    if(circle.progress() > 10){
		                    	System.out.println("Lukk avatud!");
		                    	setState(States.STATE_SHOW_CURSOR_POSITION);
		                    }
	                    } else {
	                       	System.out.println("Sa teed ringe valet pidi!");
	                    }
	            	}else{
	    				System.out.println("Sa pead tegema liigutusi nimetiss�rmega!");
	    			}   
	            }else{
	            	System.out.println("Sa pead tegema ringe, et avada lukku!");
	            }
	        }
			
	        //Siin joonistan luku avamise ja sulgemise osa
	        if(unlockProgress > 0){
		        Color color = Color.GREEN;
		        long time = System.currentTimeMillis() - unlockStateChangeTime;
		        color = new Color(color.getRed(), color.getGreen(), color.getBlue(), (int)Math.min(255, unlockProgress*150+Math.min(50, time/10)));
		        int x = (int) (screen.defaultScreen.getMaxX() - screen.defaultScreen.getX());
				int y = (int) (screen.defaultScreen.getMaxY() - screen.defaultScreen.getY());
				int size = 300;
				g.setColor(color);
				g.drawRoundRect(x/2-size/2, y/2-size/2, size, size, 50, 50);
		        g.fillRoundRect(x/2-size/2, y/2-size/2 + (size - (int)(size*unlockProgress)), size, (int)(size*unlockProgress), 50, 50);
		        
		        g.setColor(new Color(.5f, .5f, .5f, color.getAlpha()/255f));
		        g.setFont(new Font("Arial", Font.BOLD, (int)(unlockSpeed/5+30 + unlockProgress*10)));
		        String s = Math.round(unlockProgress*100f) + "%";
				g.drawString(s, x/2 - g.getFontMetrics().stringWidth(s)/2, y/2 - g.getFontMetrics().getHeight()+size/2);
	        }
		}
	}
	
	public void stateShowCursorPosition(Graphics g, Controller c){
		if(c.frame().hands().count() == 0){
			setState(States.STATE_IDLE);
		}else{
			long time = System.currentTimeMillis() - stateStartTime;
			drawCursor(g, c);
			if(time < 250){
				int size = (int) (250-time);
				g.setColor(new Color(0.2f, 1f, 0.2f, time/250f));
				g.fillOval(cur.getX()-size/2, cur.getY()-size/2, size, size);
			}else{
				int x = (int) (screen.defaultScreen.getMaxX() - screen.defaultScreen.getX());
				int y = (int) (screen.defaultScreen.getMaxY() - screen.defaultScreen.getY());
				double distFromCenter = Math.sqrt((x/2-cur.getX())*(x/2-cur.getX())  +  (y/2-cur.getY())*(y/2-cur.getY()));
				if(distFromCenter < screen.defaultScreen.getWidth()/3){
					setState(States.STATE_ENTER_MENU);
				}
			}
		}
	}
	
	public void stateEnterMenu(Graphics g, Controller c){
		long time = System.currentTimeMillis() - stateStartTime;
		g.setColor(generateButtonColor(time));
		int size = 500;
		if(time/300f < 1.5f){
			size = 200 + (int)(Math.sin(time/300f)*300);
		}
		
		int x = (int) (screen.defaultScreen.getMaxX() - screen.defaultScreen.getX());
		int y = (int) (screen.defaultScreen.getMaxY() - screen.defaultScreen.getY());
		g.fillOval(x/2-size/2, y/2-size/2, size, size);
		g.drawOval(x/2-size/2, y/2-size/2, size, size);
		
		drawCursor(g, c);
		
		double distFromCenter = Math.sqrt((x/2-cur.getX())*(x/2-cur.getX())  +  (y/2-cur.getY())*(y/2-cur.getY()));
		if(distFromCenter < 100){
			setState(States.STATE_SHOW_MAIN_MENU);
		}else if(distFromCenter > screen.defaultScreen.getWidth()/3){
			setState(States.STATE_IDLE);
		}
		if(c.frame().hands().count() == 0){
			setState(States.STATE_IDLE);
		}
	}
	
	public void stateShowMainMenu(Graphics g, Controller c){
		long time = System.currentTimeMillis() - stateStartTime;
		
		//et v�lja fade'imisel oleksid v�rvid endised
		if(currentState != States.STATE_SHOW_MAIN_MENU)	time += 5000;
		
		int x = (int) (screen.defaultScreen.getMaxX() - screen.defaultScreen.getX())/2;
		int y = (int) (screen.defaultScreen.getMaxY() - screen.defaultScreen.getY())/2;
		
		//vajadusel fade'ime keskmise ringi v�lja
		if(time/3000f < 1.5f  &&  currentState == States.STATE_SHOW_MAIN_MENU){
			int size = 278 + (int)(Math.sin(time/200f+1)*300);
			g.setColor(new Color(0, 0.02f, 0, Math.max(0, 0.1f-time/3000f)));
			g.fillOval(x-size/2, y-size/2, size, size);
			g.drawOval(x-size/2, y-size/2, size, size);
		}
		
		//samal ajal fade'ime teised elemendid sisse
		int size = 300;
		double distFromCenter = Math.sqrt((x-cur.getX())*(x-cur.getX())  +  (y-cur.getY())*(y-cur.getY()));
		Color color = generateButtonColor(time);
		float zeroToOne = (float)Math.min(1f, Math.max(0, distFromCenter - size + size/4)/60);
		Color pushColor = generatePushedButtonColor(zeroToOne, time);
		g.setColor(color);
		
		if(currentState != States.STATE_SHOW_MAIN_MENU){
			//teen aja j�lle normaalseks
			time -= 5000;
			color = new Color(color.getRed(), color.getGreen(), color.getBlue(), (int)Math.max(0, color.getAlpha() - time/2));
			pushColor = new Color(pushColor.getRed(), pushColor.getGreen(), pushColor.getBlue(), (int)Math.max(0, pushColor.getAlpha() - time/2));
			
		}
		
		//360/0 degrees is at left, 90 deg at top
		double angle = Math.toDegrees(Math.atan2(y - cur.getY(), x - cur.getX()));
		if(angle < 0) angle += 360;
		
		
		int size2 = (int) (size + zeroToOne*100);
		if(angle > 45  &&  angle < 135){
			g.drawImage(generateRoundButton(size2, size2/2, pushColor, 0, 90, Vocabulary.get(Vocabulary.MUSIC)), x-size2, y-size2, null);
			if(zeroToOne == 1  &&  currentState == States.STATE_SHOW_MAIN_MENU) setState(States.STATE_SHOW_MUSIC_MENU);
		}else{
			g.drawImage(generateRoundButton(size, size/2, color, 0, 90, Vocabulary.get(Vocabulary.MUSIC)), x-size, y-size, null);
		}
		if(angle < 45  ||  angle > 315){
			g.drawImage(generateRoundButton(size2, size2/2, pushColor, -90, 90, Vocabulary.get(Vocabulary.ROOM)), x-size2, y-size2, null);
			if(zeroToOne == 1  &&  currentState == States.STATE_SHOW_MAIN_MENU) setState(States.STATE_SHOW_ROOM_MENU);
		}else{
			g.drawImage(generateRoundButton(size, size/2, color, -90, 90, Vocabulary.get(Vocabulary.ROOM)), x-size, y-size, null);
		}
		if(angle > 135  &&  angle < 225){
			g.drawImage(generateRoundButton(size2, size2/2, pushColor, 90, 90, Vocabulary.get(Vocabulary.LOCK)), x-size2, y-size2, null);
			if(zeroToOne == 1  &&  currentState == States.STATE_SHOW_MAIN_MENU) setState(States.STATE_CONFIRM_LOCK);//TODO
		}else{
			g.drawImage(generateRoundButton(size, size/2, color, 90, 90, Vocabulary.get(Vocabulary.LOCK)), x-size, y-size, null);
		}
		
		if(angle > 225  &&  angle < 315  &&  zeroToOne > 0.5f){
			setState(States.STATE_ENTER_MENU);
		}
		
		if(currentState == States.STATE_SHOW_MAIN_MENU)
			drawCursor(g, c);
	}
	
	public void stateShowRoomMenu(Graphics g, Controller c){
		long time = System.currentTimeMillis() - stateStartTime;
		
		//laseme eelmisel aknal ennast v�lja fadeida
		if(time < 1000) stateShowMainMenu(g, c);
		
		//Joonistame uue men��
		int x = (int) (screen.defaultScreen.getMaxX() - screen.defaultScreen.getX())/2-500;
		int y = (int) (screen.defaultScreen.getMaxY() - screen.defaultScreen.getY())/2;
		
		int size = 300;
		double distFromCenter = Math.sqrt((x-cur.getX())*(x-cur.getX())  +  (y-cur.getY())*(y-cur.getY()));
		Color color = generateButtonColor(time);
		float zeroToOne = (float)Math.min(1f, Math.max(0, distFromCenter - size + size/4)/60);
		Color pushColor = generatePushedButtonColor(zeroToOne, time);
		g.setColor(color);
		
		double angle = Math.toDegrees(Math.atan2(y - cur.getY(), x - cur.getX()));
		if(angle < 0) angle += 360;
		
		int size2 = (int) (size + zeroToOne*100);
		if(angle > 45  &&  angle < 135){
			g.drawImage(generateRoundButton(size2, size2/2, pushColor, 0, 90, Vocabulary.get(Vocabulary.LIGHT)), x-size2, y-size2, null);
			if(zeroToOne == 1  &&  currentState == States.STATE_SHOW_ROOM_MENU) {
				setState(States.STATE_ENTER_MENU);
				main.room.lampOn();
			}
		}else{
			g.drawImage(generateRoundButton(size, size/2, color, 0, 90, Vocabulary.get(Vocabulary.LIGHT)), x-size, y-size, null);
		}
		if(angle > 135  &&  angle < 225){
			g.drawImage(generateRoundButton(size2, size2/2, pushColor, 90, 90, Vocabulary.get(Vocabulary.CLOSE_DOOR)), x-size2, y-size2, null);
			if(zeroToOne == 1  &&  currentState == States.STATE_SHOW_ROOM_MENU) {
				setState(States.STATE_ENTER_MENU);
				main.room.closeDoor();
			}
		}else{
			g.drawImage(generateRoundButton(size, size/2, color, 90, 90, Vocabulary.get(Vocabulary.CLOSE_DOOR)), x-size, y-size, null);
		}
		if(angle < 315  &&  angle > 225){
			g.drawImage(generateRoundButton(size2, size2/2, pushColor, 180, 90, Vocabulary.get(Vocabulary.DARK)), x-size2, y-size2, null);
			if(zeroToOne == 1  &&  currentState == States.STATE_SHOW_ROOM_MENU) {
				setState(States.STATE_ENTER_MENU);
				main.room.lampOff();
			}
		}else{
			g.drawImage(generateRoundButton(size, size/2, color, 180, 90, Vocabulary.get(Vocabulary.DARK)), x-size, y-size, null);
		}
		
		if((angle < 45  ||  angle > 315)  &&  zeroToOne > 0.5f){
			setState(States.STATE_ENTER_MENU);
		}
		
		if(currentState == States.STATE_SHOW_ROOM_MENU)
			drawCursor(g, c);
	}
	
	public void stateShowMusicMenu(Graphics g, Controller c){
long time = System.currentTimeMillis() - stateStartTime;
		
		//laseme eelmisel aknal ennast v�lja fadeida
		if(time < 1000) stateShowMainMenu(g, c);
		
		//Joonistame uue men��
		int x = (int) (screen.defaultScreen.getMaxX() - screen.defaultScreen.getX())/2;
		int y = (int) (screen.defaultScreen.getMaxY() - screen.defaultScreen.getY())/2-300;
		
		int size = 300;
		double distFromCenter = Math.sqrt((x-cur.getX())*(x-cur.getX())  +  (y-cur.getY())*(y-cur.getY()));
		Color color = generateButtonColor(time);
		float zeroToOne = (float)Math.min(1f, Math.max(0, distFromCenter - size + size/4)/60);
		Color pushColor = generatePushedButtonColor(zeroToOne, time);
		g.setColor(color);
		
		double angle = Math.toDegrees(Math.atan2(y - cur.getY(), x - cur.getX()));
		if(angle < 0) angle += 360;
		
		int size2 = (int) (size + zeroToOne*100);
		if(angle < 315  &&  angle > 225){
			g.drawImage(generateRoundButton(size2, size2/2, pushColor, 180, 90, Vocabulary.get(Vocabulary.PLAY_PAUSE)), x-size2, y-size2, null);
			if(zeroToOne == 1  &&  currentState == States.STATE_SHOW_MUSIC_MENU) {
				setState(States.STATE_ENTER_MENU);
				main.mediaPlayerController.playPause();
			}
		}else{
			g.drawImage(generateRoundButton(size, size/2, color, 180, 90, Vocabulary.get(Vocabulary.PLAY_PAUSE)), x-size, y-size, null);
		}
		if(angle < 45  ||  angle > 315){
			g.drawImage(generateRoundButton(size2, size2/2, pushColor, -90, 90, Vocabulary.get(Vocabulary.PREVIOUS)), x-size2, y-size2, null);
			if(zeroToOne == 1  &&  currentState == States.STATE_SHOW_MUSIC_MENU) {
				setState(States.STATE_ENTER_MENU);
				main.mediaPlayerController.previousTrack();
			}
		}else{
			g.drawImage(generateRoundButton(size, size/2, color, -90, 90, Vocabulary.get(Vocabulary.PREVIOUS)), x-size, y-size, null);
		}
		if(angle < 225  &&  angle > 135){
			g.drawImage(generateRoundButton(size2, size2/2, pushColor, 90, 90, Vocabulary.get(Vocabulary.NEXT)), x-size2, y-size2, null);
			if(zeroToOne == 1  &&  currentState == States.STATE_SHOW_MUSIC_MENU) {
				setState(States.STATE_ENTER_MENU);
				main.mediaPlayerController.nextTrack();
			}
		}else{
			g.drawImage(generateRoundButton(size, size/2, color, 90, 90, Vocabulary.get(Vocabulary.NEXT)), x-size, y-size, null);
		}
		
		if(angle < 135  &&  angle > 45  &&  zeroToOne > 0.1f){
			setState(States.STATE_ENTER_MENU);
		}
		
		if(currentState == States.STATE_SHOW_MUSIC_MENU)
			drawCursor(g, c);
	}
	
	public void stateConfirmLock(Graphics g, Controller c){
		long time = System.currentTimeMillis() - stateStartTime;
		
		//laseme eelmisel aknal ennast v�lja fadeida
		if(time < 1000) stateShowMainMenu(g, c);
		
		//Joonistame uue men��
		int x = (int) (screen.defaultScreen.getMaxX() - screen.defaultScreen.getX())/2+300;
		int y = (int) (screen.defaultScreen.getMaxY() - screen.defaultScreen.getY())/2;
		
		int size = 300;
		double distFromCenter = Math.sqrt((x-cur.getX())*(x-cur.getX())  +  (y-cur.getY())*(y-cur.getY()));
		Color color = generateButtonColor(time);
		float zeroToOne = (float)Math.min(1f, Math.max(0, distFromCenter - size + size/4)/60);
		Color pushColor = generatePushedButtonColor(zeroToOne, time);
		g.setColor(color);
		
		double angle = Math.toDegrees(Math.atan2(y - cur.getY(), x - cur.getX()));
		if(angle < 0) angle += 360;
		
		int size2 = (int) (size + zeroToOne*100);
		if(angle < 315  &&  angle > 225){
			g.drawImage(generateRoundButton(size2, size2/2, pushColor, 180, 90, Vocabulary.get(Vocabulary.NO)), x-size2, y-size2, null);
			if(zeroToOne == 1  &&  currentState == States.STATE_CONFIRM_LOCK) {
				setState(States.STATE_ENTER_MENU);
			}
		}else{
			g.drawImage(generateRoundButton(size, size/2, color, 180, 90, Vocabulary.get(Vocabulary.NO)), x-size, y-size, null);
		}
		if(angle < 135  &&  angle > 45){
			g.drawImage(generateRoundButton(size2, size2/2, pushColor, 0, 90, Vocabulary.get(Vocabulary.YES)), x-size2, y-size2, null);
			if(zeroToOne == 1  &&  currentState == States.STATE_CONFIRM_LOCK) {
				setState(States.STATE_LOCKED);
			}
		}else{
			g.drawImage(generateRoundButton(size, size/2, color, 0, 90, Vocabulary.get(Vocabulary.YES)), x-size, y-size, null);
		}
		
		if(((angle < 225  &&  angle > 135)  ||  (angle < 45  ||  angle > 315))  &&  zeroToOne > 0.5f){
			setState(States.STATE_ENTER_MENU);
		}
		if(currentState == States.STATE_CONFIRM_LOCK)
			drawCursor(g, c);
		
		g.setColor(Color.RED);
		g.setFont(g.getFont().deriveFont(48f));
		g.drawString(Vocabulary.get(Vocabulary.ARE_YOU_SURE), x - g.getFontMetrics().stringWidth(Vocabulary.get(Vocabulary.ARE_YOU_SURE))/2, y + g.getFontMetrics().getHeight()/2);
	}
			
	public BufferedImage generateRoundButton(int radius, int width, Color color, int angle, int size, String text){
		//l�kkab algust tekstist eemale
		int start = -angle + 90 - size/2 + 2;
		BufferedImage img = new BufferedImage (radius*2, radius*2, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = (Graphics2D) img.getGraphics();
	    g.setColor(color);
		g.fillArc(0, 0, radius*2, radius*2, start, size-4);
	    g.setComposite(AlphaComposite.getInstance(AlphaComposite.CLEAR, 0.0f));
	    g.setColor(new Color(0.5f, 0.5f, 0.5f, 0));
	    g.fillOval(width/2, width/2, radius*2 - width, radius*2 - width);
	    
	    //Joonistan nupule teksti peale
	    g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
	    g.setColor(new Color(1f, 1f, 1f, Math.min(1f, color.getAlpha()/255f*10)));
	    
	    g.setFont(g.getFont().deriveFont(48f));
	    FontMetrics metrics = g.getFontMetrics();
	    
	    //See k�ik siin on maagilisi valemeid t�is, aga v�hemalt t��tab :)
	    int extra = width/5;
	    if(angle > 90  &&  angle < 270)
	    	extra += metrics.getHeight()/2;
	    int x = (int)(radius - Math.sin(Math.toRadians(-angle))*(radius-extra));
	    int y = (int)(radius - Math.cos(Math.toRadians(-angle))*(radius-extra));
	    AffineTransform at = new AffineTransform();
	    //et tekst oleks alati �igetpidi
	    if(angle > 90  &&  angle < 270)
	    	angle += 180;
	    at.setToRotation(Math.toRadians(angle), x, y);
	    g.setTransform(at);
		g.drawString(text, x - metrics.stringWidth(text)/2, y + metrics.getHeight()/2);
	    return img;
	}

	public Color generateButtonColor(long time){
		return new Color(0.5f, 0.5f, 0.5f, Math.min(0.4f, time/2000f));
	}
	
	public Color generatePushedButtonColor(float zeroToOne, long time){
		return new Color(0.5f-zeroToOne/2f, zeroToOne/4f+0.5f, 0.5f-zeroToOne/2f, Math.max(Math.min(0.4f, time/2000f), zeroToOne));
	}
	
	private void drawCursor(Graphics g, Controller c){
		//joonistan kursori
		g.setColor(Color.GREEN);
		for(Hand hand : c.frame().hands()) {
			for(Finger finger : hand.fingers()){
				if(finger.type() == Finger.Type.TYPE_INDEX){
					cur.calculatePosition(finger);
					g.fillOval(cur.getX()-10, cur.getY()-10, 20, 20);
				}
			}
		}
	}
	
	/*private double kaugus(Vector p1, Vector p2){
		float f[] = new float[3];
		f[0] = p1.getX() - p2.getX();
		f[1] = p1.getY() - p2.getY();
		f[2] = p1.getZ() - p2.getZ();
		
		double ans = Math.sqrt(f[0]*f[0]+f[1]*f[1]+f[2]*f[2]);
		System.out.println("Kaugus k�est: " + ans + "  0: " + f[0] + "  1: " + f[1] + "  2: " + f[2]);
		return ans;
	}*/
	
}
