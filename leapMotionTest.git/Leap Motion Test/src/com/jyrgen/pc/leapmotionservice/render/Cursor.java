package com.jyrgen.pc.leapmotionservice.render;

import com.jyrgen.pc.leapmotionservice.LeapMotionTestMain;
import com.leapmotion.leap.Finger;

public class Cursor {
	private final boolean SHOULD_USE_STABLE_POSITION = false;
	private int posX, posY;
	private LeapMotionTestMain main;
	
	public Cursor(LeapMotionTestMain main) {
		this.main = main;
	}

	public int getX() {
		return posX;
	}

	public void setX(int x) {
		this.posX = x;
	}

	public int getY() {
		return posY;
	}

	public void setY(int y) {
		this.posY = y;
	}
	
	public void calculatePosition(Finger finger){
		if(SHOULD_USE_STABLE_POSITION){
			posX = (int)(finger.stabilizedTipPosition().getX()*main.sensitivity + main.screen.getWidth()/2);
			posY = (int)(((main.screen.getHeight()/main.sensitivity)-finger.stabilizedTipPosition().getY())*main.sensitivity + main.screen.getHeight()/2)+800;
		}else{
			posX = (int)(finger.tipPosition().getX()*main.sensitivity + main.screen.getWidth()/2);
			posY = (int)(((main.screen.getHeight()/main.sensitivity)-finger.tipPosition().getY())*main.sensitivity + main.screen.getHeight()/2)+800;
		}
		
		if(posX < 0) posX = 0;
		if(posY < 0) posY = 0;
		if(posX == 0  &&  posY == 0){
			main.exit();
		}
	}
	
}
