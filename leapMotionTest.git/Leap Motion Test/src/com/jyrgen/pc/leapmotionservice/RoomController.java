package com.jyrgen.pc.leapmotionservice;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RoomController {
	
	public RoomController() {
	}
	
	private boolean sendCommand(String cmd){
		System.out.println("Saadan k�su \"" + cmd + "\" \t (" + "http://192.168.1.81?add&a=" + cmd + ")");

		URL url;
		HttpURLConnection connection = null;
		try {
			// Create connection
			url = new URL("http://192.168.1.81?add&a=" + cmd);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			connection.setRequestProperty("Content-Language", "en-US");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			// Send request
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.flush();
			wr.close();

			// Get Response
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			System.out.println("Received: " + response.toString());
			return true;

		} catch (Exception e) {

			e.printStackTrace();
			return false;

		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
		
		
	}
	
	public void closeDoor(){
		sendCommand("uks_kinni");
	}
	
	public void lampOn(){
		sendCommand("lamp_sisse");
	}
	
	public void lampOff(){
		sendCommand("lamp_valja");
	}
	
	
}
