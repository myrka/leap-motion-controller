package com.jyrgen.pc.leapmotionservice;
import com.leapmotion.leap.Controller;
import com.leapmotion.leap.Controller.PolicyFlag;
import com.leapmotion.leap.Gesture;
import com.leapmotion.leap.Listener;

public class MotionListener extends Listener {
	
	private Controller controller;
	private LeapMotionTestMain main;
	
	public MotionListener(LeapMotionTestMain main) {
		this.main = main;
	}
	
	public Controller getLastControllerInfo(){
		return controller;
	}
	
    public void onInit(Controller controller) {
    	//Jooksutatakse k�ige esimesena
        System.out.println("Initialized");
        refreshScreen(controller);
    }

    public void onConnect(Controller controller) {
    	//Jooksutatakse, kui suudetakse �henduda Leap Motion Controlleriga
        System.out.println("Connected");
        controller.enableGesture(Gesture.Type.TYPE_SWIPE);
        controller.enableGesture(Gesture.Type.TYPE_CIRCLE);
        controller.enableGesture(Gesture.Type.TYPE_SCREEN_TAP);
        controller.enableGesture(Gesture.Type.TYPE_KEY_TAP);
        controller.setPolicyFlags(PolicyFlag.POLICY_BACKGROUND_FRAMES);
        
        refreshScreen(controller);
    }

    public void onDisconnect(Controller controller) {
        //Jooksutatakse siis, kui device �hendatakse arvuti k�ljest lahti
        System.out.println("Disconnected");
        refreshScreen(controller);
    }

    public void onExit(Controller controller) {
    	//k�ivitatakse ilmselt siis, kui listener eemaldatakse
        System.out.println("Exited");
        refreshScreen(controller);
    }

    int lagRemover = 0;
    public void onFrame(Controller controller) {
    	/*if(main.screen.renderer.currentState == States.STATE_IDLE){
    		if(lagRemover > 20){
    			System.out.print("IDLE");
        		lagRemover = 0;
    			refreshScreen(controller);
    		}
    	}else{*/
    	if(main.screen.renderer.shouldPaintFast){
    		refreshScreen(controller);
    	}else{
    		if(lagRemover > 50){
        		lagRemover = 0;
    			refreshScreen(controller);
    		}
    	}
    	
    	lagRemover++;
    }
    
    private void refreshScreen(Controller controller){
    	this.controller = controller;
    	main.screen.repaint();
    }
}